package com.example.acer.klinikapp;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class FragmentCreateAccount extends Fragment{

    EditText editText_last_name, editText_first_name, editText_home_address, editText_email_address ,editText_mobile_number, editText_desired_username, editText_desired_password, editText_repeat_desired_password;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup viewGroup, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_create_account, viewGroup, false);

        // note that we're looking for a button with id="@+id/myButton" in your inflated layout
        // Naturally, this can be any View; it doesn't have to be a button
        editText_last_name =(EditText) view.findViewById(R.id.EditText_last_name);
        editText_first_name =(EditText)view.findViewById(R.id.EditText_first_name);
        editText_home_address =(EditText)view.findViewById(R.id.EditText_home_address);
        editText_email_address =(EditText)view.findViewById(R.id.EditText_email_address);
        editText_mobile_number =(EditText)view.findViewById(R.id.EditText_mobile_number);
        editText_desired_username =(EditText)view.findViewById(R.id.EditText_desired_username);
        editText_desired_password =(EditText)view.findViewById(R.id.EditText_desired_password);
        editText_repeat_desired_password =(EditText)view.findViewById(R.id.EditText_repeat_desired_password);

        Button button_cancel =(Button)view.findViewById(R.id.Button_cancel);
        Button button_continue =(Button)view.findViewById(R.id.Button_continue);

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // here you set what you want to do when user clicks your button,
                // e.g. launch a new activity


                new AlertDialog.Builder(getActivity())
                        .setTitle("Confirmation")
                        .setMessage("Are you sure you want to cancel?")
                        .setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete

                                android.support.v4.app.Fragment fragment = null;
                                fragment = new FragmentLogin();
                                FragmentManager manager = getFragmentManager();
                                FragmentTransaction transaction = manager.beginTransaction();
                                transaction.replace(R.id.output, fragment);
                                transaction.commit();

                            }
                        })
                        .setNegativeButton(R.string.alert_dialog_no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(R.drawable.information)
                        .show();
            }


        });

        button_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // here you set what you want to do when user clicks your button,
                // e.g. launch a new activity


                if (editText_last_name.getText().toString().equals(""))
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Invalid")
                            .setMessage("Please provide your last name")
                            .setPositiveButton(R.string.alert_dialog_proceed, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    editText_last_name.requestFocus();
                                }
                            })
                            .setIcon(R.drawable.information)
                            .show();
                }
                else if (editText_first_name.getText().toString().equals(""))
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Invalid")
                            .setMessage("Please provide your first name")
                            .setPositiveButton(R.string.alert_dialog_proceed, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    editText_first_name.requestFocus();
                                }
                            })
                            .setIcon(R.drawable.information)
                            .show();
                }
                else if (editText_home_address.getText().toString().equals(""))
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Invalid")
                            .setMessage("Please provide your home address")
                            .setPositiveButton(R.string.alert_dialog_proceed, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    editText_home_address.requestFocus();
                                }
                            })
                            .setIcon(R.drawable.information)
                            .show();
                }
                else if (editText_email_address.getText().toString().equals(""))
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Invalid")
                            .setMessage("Please provide your email address")
                            .setPositiveButton(R.string.alert_dialog_proceed, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    editText_email_address.requestFocus();
                                }
                            })
                            .setIcon(R.drawable.information)
                            .show();
                }
                else if (editText_mobile_number.getText().toString().equals("") || editText_mobile_number.length() < 11)
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Invalid")
                            .setMessage("Please provide your valid mobile number")
                            .setPositiveButton(R.string.alert_dialog_proceed, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    editText_mobile_number.requestFocus();
                                }
                            })
                            .setIcon(R.drawable.information)
                            .show();
                }
                else if (editText_desired_username.getText().toString().equals(""))
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Invalid")
                            .setMessage("Please provide your desired username")
                            .setPositiveButton(R.string.alert_dialog_proceed, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    editText_desired_username.requestFocus();
                                }
                            })
                            .setIcon(R.drawable.information)
                            .show();
                }
                else if (editText_desired_password.getText().toString().equals(""))
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Invalid")
                            .setMessage("Please provide your desired password")
                            .setPositiveButton(R.string.alert_dialog_proceed, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    editText_desired_password.requestFocus();
                                }
                            })
                            .setIcon(R.drawable.information)
                            .show();
                }
                else if (editText_repeat_desired_password.getText().toString().equals(""))
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Invalid")
                            .setMessage("Please repeat your desired password")
                            .setPositiveButton(R.string.alert_dialog_proceed, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    editText_repeat_desired_password.requestFocus();
                                }
                            })
                            .setIcon(R.drawable.information)
                            .show();
                }
                else
                {

                    if (editText_desired_password.getText().toString().equals(editText_repeat_desired_password.getText().toString()))
                    {

                        new AlertDialog.Builder(getActivity())
                                .setTitle("Welcome")
                                .setMessage("Account created.")
                                .setPositiveButton(R.string.alert_dialog_proceed, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        android.support.v4.app.Fragment fragment = null;
                                        fragment = new FragmentLogin();
                                        FragmentManager manager = getFragmentManager();
                                        FragmentTransaction transaction = manager.beginTransaction();
                                        transaction.replace(R.id.output, fragment);
                                        transaction.commit();
                                    }
                                })
                                .setIcon(R.drawable.information)
                                .show();
                    }
                    else
                    {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Invalid")
                                .setMessage("Password does not match")
                                .setPositiveButton(R.string.alert_dialog_proceed, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        editText_desired_password.setText("");
                                        editText_repeat_desired_password.setText("");
                                        editText_desired_password.requestFocus();

                                    }
                                })
                                .setIcon(R.drawable.information)
                                .show();

                    }
                }



            }


        });
        // after you've done all your manipulation, return your layout to be shown
        return view;
    }

}