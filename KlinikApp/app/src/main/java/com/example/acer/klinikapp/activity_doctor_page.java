package com.example.acer.klinikapp;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class activity_doctor_page extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_page);

        Toolbar toolbar_doctor = (Toolbar) findViewById(R.id.toolbar_doctor);
        setSupportActionBar(toolbar_doctor);

        DrawerLayout drawer_doctor = (DrawerLayout) findViewById(R.id.drawer_layout_doctor);
        ActionBarDrawerToggle toggle_doctor = new ActionBarDrawerToggle(
                this, drawer_doctor, toolbar_doctor, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_doctor.setDrawerListener(toggle_doctor);
        toggle_doctor.syncState();

        NavigationView navigationView_doctor = (NavigationView) findViewById(R.id.nav_view_doctor);
        navigationView_doctor.setNavigationItemSelectedListener(this);

        String title = getString(R.string.nav_title_doctor_notifications);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_doctor);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_doctor_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;
        String title = getString(R.string.app_name);
        if (id == R.id.nav_doctor_appointments)
        {
            fragmentClass = FragmentCreateAccount.class;
            title  = getString(R.string.nav_title_doctor_appointments);
        }
        else if (id == R.id.nav_doctor_my_patients)
        {
            fragmentClass = FragmentCreateAccount.class;
            title  = getString(R.string.nav_title_doctor_my_patients);
        }
        else if (id == R.id.nav_doctor_notifications)
        {
            fragmentClass = FragmentCreateAccount.class;
            title  = getString(R.string.nav_title_doctor_notifications);
        }
        else if (id == R.id.nav_doctor_account_settings)
        {
            fragmentClass = FragmentCreateAccount.class;
            title  = getString(R.string.nav_title_doctor_account_settings);
        }
        else if (id == R.id.nav_doctor_logout)
        {
            fragmentClass = FragmentLogin.class;
            finish();
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_content_doctor, fragment).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_doctor);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
