package com.example.acer.klinikapp;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentClinicInformationBusinessConcept extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup viewGroup, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_clinic_information_business_concept, viewGroup, false);
        return view;
    }
    public void onResume()
    {
        super.onResume();
        // Set title bar
        ((ClinicInformation) getActivity())
                .setActionBarTitle("Business Concept");
    }
}