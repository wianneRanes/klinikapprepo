package com.example.acer.klinikapp;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class patient extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);

        Toolbar toolbar_patient = (Toolbar) findViewById(R.id.toolbar_patient);
        setSupportActionBar(toolbar_patient);

        DrawerLayout drawer_patient = (DrawerLayout) findViewById(R.id.drawer_layout_patient);
        ActionBarDrawerToggle toggle_patient = new ActionBarDrawerToggle(
                this, drawer_patient, toolbar_patient, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_patient.setDrawerListener(toggle_patient);
        toggle_patient.syncState();

        NavigationView navigationView_patient = (NavigationView) findViewById(R.id.nav_view_patient);
        navigationView_patient.setNavigationItemSelectedListener(this);


        android.support.v4.app.Fragment fragment = null;
        fragment = new FragmentCreateAccount();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_content_patient, fragment).commit();

        String title = getString(R.string.nav_title_patient_notifications);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer_patient = (DrawerLayout) findViewById(R.id.drawer_layout_patient);
        if (drawer_patient.isDrawerOpen(GravityCompat.START)) {
            drawer_patient.closeDrawer(GravityCompat.START);
        }
        else
        {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_patient, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;
        String title = getString(R.string.app_name);

        if (id == R.id.nav_patient_request_appointment)
        {
            fragmentClass = FragmentCreateAccount.class;
            title  = getString(R.string.nav_title_patient_request_appointment);
        }
        else if (id == R.id.nav_patient_notifications)
        {
            fragmentClass = FragmentCreateAccount.class;
            title  = getString(R.string.nav_title_patient_notifications);
        }
        else if (id == R.id.nav_patient_settings)
        {
            fragmentClass = FragmentCreateAccount.class;
            title  = getString(R.string.nav_title_patient_account_settings);
        }
        else if (id == R.id.nav_patient_logout)
        {
            fragmentClass = FragmentLogin.class;

            finish();
        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_content_patient, fragment).commit();

        DrawerLayout drawer_doctor = (DrawerLayout) findViewById(R.id.drawer_layout_patient);
        drawer_doctor.closeDrawer(GravityCompat.START);
        return true;
    }
}
