package com.example.acer.klinikapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class PasswordPage extends AppCompatActivity {
    TextView username;
    ImageView user_type;
    Button login;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_page);
        String string_username = (String) getIntent().getStringExtra("username");

        username = (TextView)findViewById(R.id.TextView_username);
        username.setText(string_username);
        login = (Button)findViewById(R.id.Button_log_in);
        login.setEnabled(false);
        password = (EditText)findViewById(R.id.EditText_password);
        login.setEnabled(false);
        password.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

                if (s.toString().equals("")) {
                    login.setEnabled(false);
                } else {
                    login.setEnabled(true);
                }
            }


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        if (string_username.contains("patient"))
        {
            user_type = (ImageView)findViewById(R.id.ImageView_login_logo);
            user_type.setBackgroundResource(R.drawable.user_patient);
        }

        else if (string_username.contains("doctor"))
        {
            user_type = (ImageView)findViewById(R.id.ImageView_login_logo);
            user_type.setBackgroundResource(R.drawable.user_doctor);
        }

        else if (string_username.contains("admin"))
        {
            user_type = (ImageView)findViewById(R.id.ImageView_login_logo);
            user_type.setBackgroundResource(R.drawable.user_admin);
        }

        else
        {
            Intent i = new Intent(getApplicationContext(), LogInPage.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string_password;
                String string_username = (String) getIntent().getStringExtra("username");
                string_password = password.getText().toString();

                if (string_password.equals("1") && string_username.equals("patient"))
                {
                    Intent i = new Intent(getApplicationContext(), patient.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    password.setText("");
                    finish();
                }
                else if (string_password.equals("2") && string_username.equals("doctor"))
                {
                    Intent i = new Intent(getApplicationContext(), activity_doctor_page.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    password.setText("");
                    finish();
                }
                else if (string_password.equals("admin"))
                {

                }
                else
                {
                    Intent i = new Intent(getApplicationContext(), LogInPage.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_password_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.item_clinic_information) {
            Intent i = new Intent(getApplicationContext(), ClinicInformation.class);
            // i.putExtra("sName", editName.getText().toString());
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }



}
