package com.example.acer.klinikapp;

        import android.app.ActionBar;
        import android.content.Intent;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentActivity;
        import android.support.v4.app.FragmentManager;
        import android.support.v4.app.FragmentTransaction;
        import android.support.v7.app.ActionBarActivity;
        import android.support.v7.app.AppCompatActivity;
        import android.view.Menu;
        import android.view.MenuItem;

public class ClinicInformation extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_information);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_clinic_information, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Fragment fragment = null;
        if (id == R.id.item_mission_and_vision) {
            fragment = new FragmentClinicInformationMissionAndVision();
        }
        else if (id == R.id.item_history){
            fragment = new FragmentClinicInformationHistory();
        }
        else if (id == R.id.item_business_concept){
            fragment = new FragmentClinicInformationBusinessConcept();
        }
        else if (id == R.id.item_details){
            fragment = new FragmentClinicInformationDetails();
        }
        else if (id == R.id.item_services){
            fragment = new FragmentClinicInformationServices();
        }
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.output, fragment);
        transaction.commit();
        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

}