package com.example.acer.klinikapp;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class FragmentLogin extends Fragment {



    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log_in, viewGroup, false);

        // note that we're looking for a button with id="@+id/myButton" in your inflated layout
        // Naturally, this can be any View; it doesn't have to be a button
        Button create_account = (Button) view.findViewById(R.id.Button_create_account);
        final Button next = (Button) view.findViewById(R.id.Button_Next);
        final EditText username = (EditText) view.findViewById(R.id.EditText_username);
        next.setEnabled(false);
        username.requestFocus();
        username.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

                if (s.toString().equals("")) {
                    next.setEnabled(false);
                } else {
                    next.setEnabled(true);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // here you set what you want to do when user clicks your button,
                // e.g. launch a new activity

                android.support.v4.app.Fragment fragment = null;
                fragment = new FragmentCreateAccount();
                FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.output, fragment);
                transaction.commit();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sUserName;
                sUserName = username.getText().toString();

                if (sUserName.equals("")) {

                }
                else
                {
                    Intent intent = new Intent(getActivity(), PasswordPage.class);
                    intent.putExtra("username", username.getText().toString());
                    startActivity(intent);
                    username.setText("");
                }
            }
        });




        // after you've done all your manipulation, return your layout to be shown
        return view;

    }

}

